package com.uearn.qa.utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.uearn.qa.resource.ExcelDataConfig;

public class DataProviderConcept {

   ExcelDataConfig edc=new ExcelDataConfig();
	
	  @DataProvider(name="xldataprovider")
	public String[][] getData() throws IOException{
// read data from excel
		String path=System.getProperty("user.dir")+"";
		int rownum=edc.getRowCount(path,"sheet1");
		int colcount=edc.getCellCount(path,"sheet1",1);
// array length defining		
		String empdata[][]=new String[rownum][colcount];
		
		for(int i=1;i<=rownum;i++) {
			for(int j=0;j<colcount;j++) {
			empdata[i-1][j]=edc.getCellData(path,"sheet1",i,j);
		}
	   }
		return empdata;
	}

 // below is utilized in test level or if we use parameterized then it will use in xml file	  
	  @Parameters({"rownumber"})
	  @DataProvider(name="getTestData")
	  public Iterator<Object[]> getTestData(int rownumber) throws IOException{
		  ArrayList<Object[]> testdata=edc.getDataFromExcel(1);
		  return testdata.iterator();
	  }

	
}
