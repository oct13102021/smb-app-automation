package com.uearn.qa.resource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.testng.annotations.Test;

public class DB_Operations {

	public synchronized HashMap<String,String> getSqlResultInMap(String sql){
		HashMap<String,String> data_map=new HashMap<String,String>();
		String url="jdbc:sqlserver://13.234.200.246:1433;database=gcp_prod_db";
		String user="sa";
		String password="P3K79Lk-2LPZVAq@";
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con=DriverManager.getConnection(url,user,password);
			Statement st=con.createStatement();
			Thread.sleep(6000);
			ResultSet rs=st.executeQuery(sql);
			Thread.sleep(4000);
			ResultSetMetaData rsmd=rs.getMetaData();
			while(rs.next()) {
				for(int i=1;i<=rsmd.getColumnCount();i++) {
					data_map.put(rsmd.getColumnName(i), rs.getString(i));
				}
			}
			System.out.println(data_map);
			rs.close();
			st.close();
			con.close();
			}
			catch(Exception e) {
				System.out.println("error"+e);
			}

		return data_map; 
	}
	
	public synchronized void updateSqlQuery(String sql) {
		String url="jdbc:sqlserver://13.234.200.246:1433;database=gcp_prod_db";
		String user="sa";
		String password="P3K79Lk-2LPZVAq@";
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con=DriverManager.getConnection(url,user,password);
			Statement st=con.createStatement();
			Thread.sleep(6000);
			st.executeUpdate(sql);
		
			st.close();
			con.close();
			}
			catch(Exception e) {
				System.out.println("error"+e);
			}
	}
	
	public synchronized List<String> getSqlResultInList(String sql){
		List<String> data_list=new ArrayList<String>();
		String url="jdbc:sqlserver://13.234.200.246:1433;database=gcp_prod_db";
		String user="sa";
		String password="P3K79Lk-2LPZVAq@";
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con=DriverManager.getConnection(url,user,password);
			Statement st=con.createStatement();
			Thread.sleep(6000);
			ResultSet rs=st.executeQuery(sql);
			Thread.sleep(4000);
			ResultSetMetaData rsmd=rs.getMetaData();
			Thread.sleep(4000);
			while(rs.next()) {
				for(int i=1;i<=rsmd.getColumnCount();i++) {
					data_list.add(rs.getString(i));
				}
			}
			System.out.println("added data");
			System.out.println(data_list);
			rs.close();
			st.close();
			con.close();
			}
			catch(Exception e) {
				System.out.println("error"+e);
			}

		return data_list; 
	}
}
