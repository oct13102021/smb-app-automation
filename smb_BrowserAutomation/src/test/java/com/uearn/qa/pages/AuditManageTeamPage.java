package com.uearn.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.uearn.qa.base.BasePage;

public class AuditManageTeamPage extends BasePage{


	@FindBy(xpath="//*[text()=\"Manage Team\"]")
	WebElement manage_team_module;

	@FindBy(xpath="//*[text()=\"account_circle\"]")
	WebElement profile_icon_button;
	
	@FindBy(xpath="//*[text()=\"Logout\"]")
	WebElement  logout_button;
	
	 public AuditManageTeamPage() {
		PageFactory.initElements(driver ,  this);
	}
	
	public void click_on_logout() throws InterruptedException {
		profile_icon_button.click();
		Thread.sleep(1000);
		logout_button.click();
	}
	
	public void click_on_manageTeam_module() {
		manage_team_module.click();
	}
}
