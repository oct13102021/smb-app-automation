package com.uearn.qa.pages;

import org.junit.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetAPIRecord {


    public RequestSpecification httpRequest;
	public Response response;
		
	// @BeforeClass
	public void getrecord(String uri) throws InterruptedException{
		RestAssured.baseURI="";
		httpRequest=RestAssured.given();
		response=httpRequest.request(Method.GET,uri);
		Thread.sleep(3000);
	}
	
	public void checkResponseBody() {
		String responseBody=response.getBody().asString();
		Assert.assertTrue(responseBody!=null);
	}
	
	public void checkStatusCode() {
		int statusCode=response.getStatusCode();
		Assert.assertEquals(statusCode, 200);
	}
	
	public void checkResponseTime(long time) {
		long responseTime=response.getTime();
		if(responseTime>time) {
			System.out.println("response time is greater than 2000");
		}
		Assert.assertTrue(responseTime<time);
	}
	
	public void checkStatusLine(String expectedSL) {
		String statusLine=response.getStatusLine();
		Assert.assertEquals(statusLine, expectedSL);
	}
	
	public void checkContentType(String expectedCT) {
		String contentType=response.header("content-Type");
		Assert.assertEquals(contentType,expectedCT);
	}
	
	public void checkServerType(String expectedST) {
		String serverType=response.header("server");
		Assert.assertEquals(serverType,expectedST);
	}
	
	public void checkContentEncoding(String expectedCE) {
		String contentEncoding=response.header("Content-Encoding");
		Assert.assertEquals(contentEncoding,expectedCE);
	}
	
	public void checkContentLength(long time) {
		String contentLength=response.header("Content-Length");
		if(Integer.parseInt(contentLength)<time) {
			System.out.println("content Length is lessthan "+time);
		}
		Assert.assertTrue(Integer.parseInt(contentLength)>time);
	}
	
	public void checkCookies(String expectedcookie) {
		String cookie=response.getCookie("");
		Assert.assertEquals(cookie,expectedcookie);
	}
	
	@AfterClass
	public void tearDown() {
		System.out.println("get record class execution completed");
	}

}
