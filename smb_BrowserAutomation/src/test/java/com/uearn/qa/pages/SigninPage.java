package com.uearn.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.uearn.qa.base.BasePage;

public class SigninPage extends BasePage{

	@FindBy(xpath="/html/body/app-root/app-signin/body/div/form/div/input[1]")
	WebElement email;
	
	@FindBy(xpath="/html/body/app-root/app-signin/body/div/form/div/input[2]")
	WebElement password;
	
	@FindBy(xpath="/html/body/app-root/app-signin/body/div/form/div/button")
	WebElement login_button;
	
	@FindBy(xpath="/html/body/app-root/app-signin/body/div/form/div/p[1]/span[2]")
	WebElement click_here_to_recover_link;
	
	@FindBy(xpath="/html/body/app-root/app-signin/body/div/form/div/p[2]/span[2]")
	WebElement register_now_link;
	
	public SigninPage() {
		PageFactory.initElements(driver , this);
	}
	
	public void enter_Email(String emails) {
		email.sendKeys(emails);
		}
	
	public void enter_Password(String passwords) {
		password.sendKeys(passwords);
	}
	
	public void click_on_Login() {
		login_button.click();
	}
	
}
