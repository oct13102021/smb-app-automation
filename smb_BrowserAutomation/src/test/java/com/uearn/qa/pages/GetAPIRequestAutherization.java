package com.uearn.qa.pages;

import org.testng.Assert;
import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetAPIRequestAutherization {

    public RequestSpecification httpRequest;
	public Response response;
	
	public void getAuthorizationRequest(String userName,String password,String uri) {
		// specify base uri
		RestAssured.baseURI="";
		
		// Basic authentication
		PreemptiveBasicAuthScheme authscheme=new PreemptiveBasicAuthScheme();
		authscheme.setUserName(userName);
		authscheme.setPassword(password);
		
		RestAssured.authentication=authscheme;
		
		// Request Object
		 httpRequest=RestAssured.given();
		
		// Response Object
		 response=httpRequest.request(Method.GET,uri);
	}
	public void getAuthBodyResponse() {
		// print Response in console window
		String responseBody=response.getBody().asString();
		System.out.println("Response Body is : "+responseBody);
	}
	
	public void checkGetAuthStatuscode() {
		// status code validation
		int statuscode=response.getStatusCode();
		System.out.println("Statuscode is : "+statuscode);
		Assert.assertEquals(statuscode,200);
	}
}
