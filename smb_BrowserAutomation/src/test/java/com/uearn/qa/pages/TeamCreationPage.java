package com.uearn.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.uearn.qa.base.BasePage;
public class TeamCreationPage extends BasePage {
	
	@FindBy(xpath="//*[text()=\"account_circle\"]")
	WebElement profile_icon_button;
	
	@FindBy(xpath="//*[text()=\"Logout\"]")
	WebElement  logout_button;

	@FindBy(xpath="//*[@id=\"managementTeam\"][text()=\"Manage Team\"]")
	WebElement manage_team_field;
	
	@FindBy(xpath="//*[@class=\"mat-focus-indicator btnAdd mat-button mat-button-base\"]")
	WebElement add_profile_button;
	
	@FindBy(xpath="//*[@placeholder=\"Name\"]")
	WebElement name_field;
	
	@FindBy(xpath="//*[@placeholder=\"Email\"]")
	WebElement email_field;
	
	@FindBy(xpath="//*[@placeholder=\"Mobile Number\"]")
	WebElement mobile_number_field;
	
	@FindBy(xpath="//*[@class=\"mat-button-wrapper\"][text()=\"Save\"]")
	WebElement save_button;
	
	@FindBy(xpath="//*[@id=\"mat-select-1\"]/div/div[1]")
	WebElement select_tl_to_add_agent;
	
	@FindBy(xpath="//*[@placeholder=\"Role\"]/div/div[1]")
	WebElement change_role_of_agent;
	
	@FindBy(xpath="//*[@class=\"mat-option-text\"][text()=\"Tele Caller\"]")
	WebElement change_role_of_agent_to_telecaller;
	
	@FindBy(xpath="//*[@class=\"mat-option-text\"][text()=\"Non Tele Caller\"]")
	WebElement  change_role_of_agent_to_non_telecaller;
	
	@FindBy(xpath="//*[@class=\"mat-option-text\"][text()=\"Admin\"]")
	WebElement  change_role_of_agent_to_admin;
	
	@FindBy(xpath="//*[@class=\"mat-option-text\"][text()=\"Data Manager\"]")
	WebElement  change_role_of_agent_to_data_manager;
	
	@FindBy(xpath="//*[@class=\"mat-option-text\"][text()=\"Super Admin\"]")
	WebElement  change_role_of_agent_to_super_admin;

	@FindBy(xpath="//*[@placeholder=\"Profile Status\"]/div/div[1]")
	WebElement  change_profile_status_of_agent;
	
	// remaining pending
	
	@FindBy(xpath="//*[@class=\"mat-option-text\"][text()=\"Training\"]")
	WebElement agent_profile_status_change_to_training;
	
	@FindBy(xpath="//*[@class=\"mat-option-text\"][text()=\"Project\"]")
	WebElement tl_profile_status_change_to_project;
	
	// remaining
	
	@FindBy(xpath="//*[@class=\"mat-button-wrapper\"][text()=\"Save\"]")
	WebElement tl_agent_edit_profile_save_button;
	
	@FindBy(xpath="//*[@aria-label=\"Next page\"]/span")
	WebElement   nextpage_button;
	
	@FindBy(xpath="//*[@class=\"mat-paginator-range-label\"]")
	WebElement no_of_tl_or_agents_present;
	
	public TeamCreationPage() {
	PageFactory.initElements(driver,this);
	}
	
	public void click_on_logout() throws InterruptedException {
		profile_icon_button.click();
		Thread.sleep(1000);
		logout_button.click();
	}
	
	// click on manage team
	public void click_on_manageteam() throws InterruptedException {
		Thread.sleep(4000);
		manage_team_field.click();
		Thread.sleep(4000);
		
	}
	
	public void add_new_tl(String name,String email,String mobile_number) throws InterruptedException {
		 manage_team_field.click();
		 Thread.sleep(4000);
		add_profile_button.click();
		name_field.sendKeys(name);
		email_field.sendKeys(email);
		mobile_number_field.sendKeys(mobile_number);
		save_button.click();
		Thread.sleep(2000);
	}
	
	public void select_tl_to_add_a_new_agent(String tlname) {
		select_tl_to_add_agent.click();
    WebElement elem=driver.findElement(By.xpath("//*[@class=\"mat-option-text\"][text()=\""+tlname+"\"]"));
    JavascriptExecutor js1=(JavascriptExecutor)driver;
    js1.executeScript("arguments[0].scrollIntoView(true);", elem);
    elem.click();
	}
	
	public void change_the_role_of_tl(String tlName) throws InterruptedException {
		int flag=0;
		 Thread.sleep(2000);
		 manage_team_field.click();
		 Thread.sleep(4000);
		
		String str=no_of_tl_or_agents_present.getText();
		String[] str1=str.split("of ");
		int a=Integer.parseInt(str1[1]);
   	 System.out.println(a);
   	 double b=(a/5.0);
   	 double d=Math.ceil(b);
   	 int c=(int)d;
   	 for(int m=1;m<=c;m++) {
        
        for(int i=1;i<=5;) {
     	  try {
     		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[2]"));
     	  
     		  WebDriverWait wait= new WebDriverWait(driver,10);
     	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[2]")));
     	  String str2=ele1.getText();
     	System.out.println(str2);
     	if(str2.equalsIgnoreCase(tlName)) {
     		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[9]")).click();
     		change_role_of_agent.click();
     		 change_role_of_agent_to_non_telecaller.click();
     	
     		 Thread.sleep(2000); 
     		change_profile_status_of_agent.click();
  	      ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", tl_profile_status_change_to_project);
  	        Thread.sleep(500);
  	      tl_profile_status_change_to_project.click();
  	      tl_agent_edit_profile_save_button.click();
  	      Thread.sleep(2000);
  	      flag=1;
  	      
  	        break;
     	}
     	i=i+1;
       }
       catch(Exception e) {
     		 // System.out.println("element not present");
     		 // break;
     	  }
     	   
        }
		if(flag==1) {
			break;
		}
		Thread.sleep(4000);
		nextpage_button.click();
   	 }
	}
	
	public void move_to_tl_nextpage(){
		try {
	      nextpage_button.click();
	       }
       catch(Exception e) {
    	   System.out.println("element not found");
       }
	}
	
	
	public void add_new_agent(String name,String email,String mobile_number) throws InterruptedException {
		// manage_team_field.click();
		// Thread.sleep(4000);
		add_profile_button.click();
		name_field.sendKeys(name);
		email_field.sendKeys(email);
		mobile_number_field.sendKeys(mobile_number);
		save_button.click();
		Thread.sleep(2000);
	}
	
	
	public void change_the_role_of_agent(String agentName) throws InterruptedException {
		int flag_agent=0;
		String str=no_of_tl_or_agents_present.getText();
		String[] str1=str.split("of ");
		int a=Integer.parseInt(str1[1]);
   	 System.out.println(a);
   	 double b=(a/5.0);
   	 double d=Math.ceil(b);
   	 int c=(int)d;
   	 for(int m=1;m<=c;m++) {
   		 
   		 for(int i=1;i<=5;) {
     	   try {
     		   WebElement ele1=driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[2]"));
     	  
     		  WebDriverWait wait= new WebDriverWait(driver,10);
     	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[2]")));
     	  String str3=ele1.getText();
     	System.out.println(str3);
     	if(str3.equalsIgnoreCase(agentName)) {
     		flag_agent=1;
     		driver.findElement(By.xpath("//*[@role=\"rowgroup\"]/tr["+i+"]/td[9]")).click();
     		change_role_of_agent.click();
     		 change_role_of_agent_to_telecaller.click();
     	
     		 Thread.sleep(2000); 
     		change_profile_status_of_agent.click();
              // WebElement element1 = driver.findElement(By.xpath("//*[@class=\"mat-option-text\"][text()=\"Training\"]"));
  	        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", agent_profile_status_change_to_training);
  	        Thread.sleep(500);
  	        agent_profile_status_change_to_training.click();
  	        tl_agent_edit_profile_save_button.click();
  	        Thread.sleep(2000);
  	        break;
     	}
        i=i+1;
     	  }
     	  catch(Exception e) {
     		//  System.out.println("element not present");
     		//  break;
     	  }
     	   
        }
        if(flag_agent==1) {
			break;
		}
		Thread.sleep(4000);
		nextpage_button.click();
        
   	 }
	 }
	
	}

