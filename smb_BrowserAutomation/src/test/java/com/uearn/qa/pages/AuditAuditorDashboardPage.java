package com.uearn.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.uearn.qa.base.BasePage;

public class AuditAuditorDashboardPage extends BasePage {


	@FindBy(xpath="//*[text()=\"Auditor Dashboard\"]")
	WebElement auditor_dashboard_module;

	@FindBy(xpath="//*[text()=\"account_circle\"]")
	WebElement profile_icon_button;
	
	@FindBy(xpath="//*[text()=\"Logout\"]")
	WebElement  logout_button;
	
	 public AuditAuditorDashboardPage() {
		PageFactory.initElements(driver ,  this);
	}
	
	public void click_on_logout() throws InterruptedException {
		profile_icon_button.click();
		Thread.sleep(1000);
		logout_button.click();
	}
	
	public void click_on_auditorDashboard_module() {
		auditor_dashboard_module.click();
	}
}
