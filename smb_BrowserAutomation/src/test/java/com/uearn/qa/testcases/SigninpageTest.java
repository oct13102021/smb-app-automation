package com.uearn.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.uearn.qa.base.BasePage;
import com.uearn.qa.pages.HomePage;
import com.uearn.qa.pages.SigninPage;
import com.uearn.qa.resource.ExcelDataConfig;

public class SigninpageTest extends BasePage{
	
     HomePage homepage;
     ExcelDataConfig edc;
     SigninPage signinpage;
     
     public SigninpageTest(){
    	 super();
     }
     
     @BeforeMethod
     public void setup() {
    	 
    	 initialization();
    	 driver.get(prop.getProperty("Team_Training_url"));
    	 homepage=new HomePage();
    	 signinpage=new SigninPage();
    	 edc=new ExcelDataConfig();
    	 
         }
     
     @Test
     public void signin_with_creadentials() {
    	 homepage.click_on_Enterprise_Login();
    	 signinpage.enter_Email(prop.getProperty("userEmail"));
    	 signinpage.enter_Password(prop.getProperty("password"));
    	 signinpage.click_on_Login();
    	 
     }
     
     @AfterMethod
     public void teardown() throws InterruptedException {
    	 Thread.sleep(4000);
    	 driver.quit();
     }

}
