package com.uearn.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.uearn.qa.base.BasePage;
import com.uearn.qa.pages.AuditManageTeamPage;
import com.uearn.qa.pages.AuditcallCallAuditPage;
import com.uearn.qa.pages.HomePage;
import com.uearn.qa.pages.SigninPage;
import com.uearn.qa.resource.ExcelDataConfig;

public class AuditcallCallAuditTest extends BasePage{

	HomePage homepage;
    ExcelDataConfig edc;
    SigninPage signinpage;
    AuditcallCallAuditPage auditcallcallauditpage;
    
    public AuditcallCallAuditTest(){
    	super();
    }
    
    @BeforeMethod
    public void setup() {
   	 
   	 initialization();
   	 driver.get(prop.getProperty("Team_Training_url"));
   	 homepage=new HomePage();
   	 signinpage=new SigninPage();
   	auditcallcallauditpage=new AuditcallCallAuditPage();
   	 edc=new ExcelDataConfig();
   	 
        }
    @Test(priority = 0,enabled = true)
    public void signin_with_creadentials() throws InterruptedException {
   	 homepage.click_on_Enterprise_Login();
   	 signinpage.enter_Email(prop.getProperty("auditcallUserEmail"));
   	 signinpage.enter_Password(prop.getProperty("auditcallPassword"));
   	 signinpage.click_on_Login();
   	 Thread.sleep(2000);
    }
    @Test(priority = 1,enabled = true)
    public void cl() throws InterruptedException {
   	 homepage.click_on_Enterprise_Login(); 
   	 signinpage.enter_Email(prop.getProperty("auditcallUserEmail"));
   	 signinpage.enter_Password(prop.getProperty("auditcallPassword"));
   	 signinpage.click_on_Login();
   	 Thread.sleep(2000);
   	auditcallcallauditpage.click_on_callAudit_module();
    }
    @AfterMethod
    public void teardown() throws InterruptedException {
   	 Thread.sleep(4000);
   	auditcallcallauditpage.click_on_logout();
   	 driver.quit();
    }

}
