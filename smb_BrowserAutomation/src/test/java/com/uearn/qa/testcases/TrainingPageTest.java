package com.uearn.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.uearn.qa.base.BasePage;
import com.uearn.qa.pages.HomePage;
import com.uearn.qa.pages.SigninPage;
import com.uearn.qa.pages.TrainingPage;
import com.uearn.qa.resource.ExcelDataConfig;

public class TrainingPageTest extends BasePage {


	 HomePage homepage;
    ExcelDataConfig edc;
    SigninPage signinpage;
    TrainingPage trainingpage;
    
    public TrainingPageTest() {
    	super();
    }
    
    @BeforeMethod
    public void setup() {
   	 
   	 initialization();
   	 driver.get(prop.getProperty("Team_Training_url"));
   	 homepage=new HomePage();
   	 signinpage=new SigninPage();
   	 trainingpage=new TrainingPage();
   	 edc=new ExcelDataConfig();
   	 
        }
    
    @Test(priority =0,enabled = true)
    public void signin_with_creadentials() throws InterruptedException {
   	 homepage.click_on_Enterprise_Login();
   	 signinpage.enter_Email(prop.getProperty("userEmail"));
   	 signinpage.enter_Password(prop.getProperty("password"));
   	 signinpage.click_on_Login();
   	 Thread.sleep(2000);
    }
    
      // trainingpage.select_training_module();
    
     @Test(priority = 1,enabled = true)
   	 public void select_a_client() throws InterruptedException {
   	   	 homepage.click_on_Enterprise_Login();
   	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
   	   	 signinpage.enter_Password(prop.getProperty("password"));
   	   	 signinpage.click_on_Login();
   	   	 Thread.sleep(4000);
   	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
   	 }
   	 @Test(priority = 2,enabled = true)
     public void select_a_agent_to_see_details_in_agents_field() throws InterruptedException {
   	   	 homepage.click_on_Enterprise_Login();
   	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
   	   	 signinpage.enter_Password(prop.getProperty("password"));
   	   	 signinpage.click_on_Login();
   	   	 Thread.sleep(4000);
   	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
       	 // agents field
      	 trainingpage.select_agents_field("ss1");
     }
   	 @Test(priority = 3,enabled = true)
     public void search_for_agent_in_agents_field() throws InterruptedException {
   	   	 homepage.click_on_Enterprise_Login();
   	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
   	   	 signinpage.enter_Password(prop.getProperty("password"));
   	   	 signinpage.click_on_Login();
   	   	 Thread.sleep(4000);
   	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
   	     // training agents search button
   	     trainingpage.search_agent_in_agents("ss1");
     }
   	 @Test(priority = 4,enabled = true)
   	 public void create_a_new_batch_in_batch_field() throws InterruptedException {
   	   	 homepage.click_on_Enterprise_Login();
   	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
   	   	 signinpage.enter_Password(prop.getProperty("password"));
   	   	 signinpage.click_on_Login();
   	   	 Thread.sleep(4000);
   	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
   		 trainingpage.select_Batch_field(2021,2021,"JAN","JAN",6,8,"09:00:00",120,45 ,new String[]{"agent2sa@test.com","agent1san@test.com"},"santhosh1","hyd");
   	 }
   	 @Test(priority = 5,enabled = true)
   	 public void edit_batch_in_batch_field() throws InterruptedException {
   	   	 homepage.click_on_Enterprise_Login();
   	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
   	   	 signinpage.enter_Password(prop.getProperty("password"));
   	   	 signinpage.click_on_Login();
   	   	 Thread.sleep(4000);
   	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
   	     // batch edit
      	 trainingpage.edit_batch_based_on_batchCode(52);
   	 }
   	@Test(priority = 6,enabled = false)
   	 public void attendance_of_agents_batch_wise_in_batchField() throws InterruptedException {
   	   	 homepage.click_on_Enterprise_Login();
   	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
   	   	 signinpage.enter_Password(prop.getProperty("password"));
   	   	 signinpage.click_on_Login();
   	   	 Thread.sleep(4000);
   	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
   		 // attendance of batch   	 
   	     trainingpage.attendance_of_agents_batch_wise(59,"shiva66");
   	 }
   	@Test(priority = 7,enabled = true)
   	public void send_meeting_link_to_batch_in_batchField() throws InterruptedException {
  	   	 homepage.click_on_Enterprise_Login();
  	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
  	   	 signinpage.enter_Password(prop.getProperty("password"));
  	   	 signinpage.click_on_Login();
  	   	 Thread.sleep(4000);
  	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
   	     trainingpage.send_meeting_link_to_batch(59, "meetingLinkDemo");
    	}
   	@Test(priority = 8,enabled = true)
   	public void  total_trainers_present_in_trainers_field() throws InterruptedException {
 	   	 homepage.click_on_Enterprise_Login();
 	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
 	   	 signinpage.enter_Password(prop.getProperty("password"));
 	   	 signinpage.click_on_Login();
 	   	 Thread.sleep(4000);
 	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
    	 trainingpage.trainers_field("Trainer 3");
   	}
   	@Test(priority = 9,enabled = true)
   	public void availability_of_trainers_in_between_specified_dates() throws InterruptedException {
 	   	 homepage.click_on_Enterprise_Login();
 	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
 	   	 signinpage.enter_Password(prop.getProperty("password"));
 	   	 signinpage.click_on_Login();
 	   	 Thread.sleep(4000);
 	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
    	 trainingpage.availability_of_trainers(2021,"JAN",6,2021,"JAN",8,"Trainer 6");
   	}
   	@Test(priority = 10,enabled = true)
   	public void search_for_trainer_in_trainers_field() throws InterruptedException {
 	   	 homepage.click_on_Enterprise_Login();
 	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
 	   	 signinpage.enter_Password(prop.getProperty("password"));
 	   	 signinpage.click_on_Login();
 	   	 Thread.sleep(4000);
 	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
    	 trainingpage.search_trainer("Trainer 6");
   	}
   	@Test(priority = 11,enabled = true)
   	public void create_new_ojt_batch() throws InterruptedException {
	   	 homepage.click_on_Enterprise_Login();
	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
	   	 signinpage.enter_Password(prop.getProperty("password"));
	   	 signinpage.click_on_Login();
	   	 Thread.sleep(4000);
	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
     	trainingpage.select_ojt_batch_field(2021, "JAN", 6, 2021, "JAN",8, "10:00:00", 120, 45, new String[]{"Smart.6111003@fkvoice.com"}, "santhosh1", "hyd");
  }  
    @Test(priority = 12,enabled = true)
    public void edit_ojt_batch_details() throws InterruptedException {
	   	 homepage.click_on_Enterprise_Login();
	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
	   	 signinpage.enter_Password(prop.getProperty("password"));
	   	 signinpage.click_on_Login();
	   	 Thread.sleep(4000);
	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
         trainingpage.ojt_batch_editBatchDetails(45);
    }
    
    @Test(priority = 13,enabled = true)
    public void viewDetails_of_agent_present_in_ojtAgents() throws InterruptedException {
	   	 homepage.click_on_Enterprise_Login();
	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
	   	 signinpage.enter_Password(prop.getProperty("password"));
	   	 signinpage.click_on_Login();
	   	 Thread.sleep(4000);
	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
    	trainingpage.viewDetails_of_on_job_training_agents("agent3");
    }
    @Test(priority = 14,enabled = true)
    public void search_for_agent_in_ojtAgents() throws InterruptedException {
	   	 homepage.click_on_Enterprise_Login();
	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
	   	 signinpage.enter_Password(prop.getProperty("password"));
	   	 signinpage.click_on_Login();
	   	 Thread.sleep(4000);
	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND");
    	 trainingpage.search_for_agent("agent3 FK");
    }
    @Test(priority = 15,enabled = true)
    public void certification_of_batch_agent_wise() throws InterruptedException {
	   	 homepage.click_on_Enterprise_Login();
	   	 signinpage.enter_Email(prop.getProperty("userEmail"));
	   	 signinpage.enter_Password(prop.getProperty("password"));
	   	 signinpage.click_on_Login();
	   	 Thread.sleep(4000);
	   	 trainingpage.select_a_Client_method("CLIENT1 - INBOUND"); 
    	trainingpage.certification_for_batch("Batch 52(Regular)","agent2sa","Certified");
    }
    
    
    @AfterMethod
    public void teardown() throws InterruptedException {
   	 Thread.sleep(4000);
   	 trainingpage.click_on_logout();
   	 Thread.sleep(2000);
   	 driver.quit();
    }
	
}
