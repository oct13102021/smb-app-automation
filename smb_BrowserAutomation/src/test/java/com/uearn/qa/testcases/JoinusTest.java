package com.uearn.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.uearn.qa.base.BasePage;
import com.uearn.qa.pages.JoinusPage;
import com.uearn.qa.resource.ExcelDataConfig;
@Listeners(com.uearn.qa.ExtentReportListener.ExtentReporterNG.class)
public class JoinusTest extends BasePage {
	
	JoinusPage joinuspage;
	ExcelDataConfig edc;
	
	public JoinusTest() {
		super();
	}
    
	@BeforeMethod
	public void setup() {
		initialization();
	    driver.get(prop.getProperty("joinus_url"));
		joinuspage=new JoinusPage();
		edc=new ExcelDataConfig();
	}
	
	@Test
	public void joinus_button_click() {
		joinuspage.enter_fullName("san22");
		joinuspage.enter_mobile_number(1232267890);
		joinuspage.enter_email("santhosh22@gmail.com");
		joinuspage.enter_password("12345678");
		joinuspage.select_gender("Male");
		// joinuspage.select_languages("English");
		joinuspage.select_languages("Telugu");
		joinuspage.select_educational_qualifications("Graduate");
		joinuspage.select_work_experience(0);
		joinuspage.select_experience_in_customer_support("No");
		joinuspage.select_business_process("Voice");
		joinuspage.select_experience_in_voice_support(0);
		joinuspage.select_choose_your_set_up("Android Phone");
		joinuspage.click_on_joinus();
		joinuspage.display_message();
	}
	
	@AfterMethod
	public void teardown() {
		driver.quit();
	}
}
