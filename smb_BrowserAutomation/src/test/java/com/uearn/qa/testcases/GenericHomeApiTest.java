package com.uearn.qa.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.uearn.qa.pages.GetAPIRecord;
import com.uearn.qa.pages.PostAPIRecord;

public class GenericHomeApiTest {


	GetAPIRecord getapirecord;
	
	@BeforeClass
	public void startup() {
		System.out.println("******* GET API RECORD TEST STARTED*******");
		getapirecord=new GetAPIRecord();
	}
	
	@Test
	public void login() throws InterruptedException {
		getapirecord.getrecord("https://web.youearn.in/");
		getapirecord.checkResponseBody();
		getapirecord.checkStatusCode();
	}
	
	@AfterClass
	public void tearDown() {
		System.out.println("********* END OF GET API RECORD TEST *********");
	}
}
